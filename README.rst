Histograms from HqT
---------
Setup ROOT::

 . setup_processHistos.sh

Produce histograms from HqT output files (listed in ``list_inputs``)::

  ./runAll.sh list_inputs

Compile code to process histograms from HqT output files and run it::

  . make.sh
  ./processHistos

An output ROOT file is produced containing the SM+EFT/SM ratios.

Clone TRexFitter, setup environment and compile
---------
I.e. ::

  git clone https://:@gitlab.cern.ch:8443/TRExStats/TRExFitter.git
  cd TRExFitter
  git checkout TtHFitter-00-03-26
  source setup.sh
  make
  ln -s ../ggH_fit.config .
  ln -s ../s_125_15_t_t   .

Run the fit
---------
Run the example fit of a Higgs pT spectrum::

  ./myFit.exe nwf ggH_fit.config

Follow TRexFitter README (``TRexFitter/README.rst``) file for detailed instructions.


