#!/bin/bash

export DIPLAY=:10.0

listtxt=$*
while read sample
do
    echo "Running input:"
    echo ${sample}
    root -l -b <<EOF
    .x readMoreHqT.C("${sample}")
    .q
EOF
    echo "Next one"
done < $*

