/////////////////////// -*- C++ -*- /////////////////////////////
// Author: A.Sciandra <andrea.sciandra@cern.ch>
/////////////////////////////////////////////////////////////////

#include "TFile.h"
#include "TTree.h"
#include "TH1.h"
#include "TString.h"
#include <vector>
#include <string>
#include <ctime>
#include <iostream>
#include <fstream>
#include <sstream>

int main() {
  // flags
  bool debug(false);//true);
  // names
  TString hist_name("hXS");

  clock_t begin       = clock();

  //Define input folder
  TString inpFold = "./";
  //Root files for looping
  const char*      files[] = {"hist-sm.root","hist-cg_001.root","hist-cg_010.root","hist-cg_025.root","hist-cg_050.root","hist-cg_075.root","hist-cg_002.root","hist-cg_003.root","hist-cg_004.root","hist-cg_005.root","hist-cg_007.root","hist-cg_015.root"};

  std::cout<<std::endl;
  std::cout<<"   ///////////////////////// -*- C++ -*- /////////////////////////////"<<std::endl;
  std::cout<<"  // processHistos.cxx                                             //"<<std::endl;
  std::cout<<" // Author: A.Sciandra <andrea.sciandra@cern.ch>                  //"<<std::endl;
  std::cout<<"///////////////////////////////////////////////////////////////////"<<std::endl;
  std::cout<<std::endl;

  const int                   NMAX = sizeof(files) / sizeof(files[0])+4;
  const int                   N = (sizeof(files) / sizeof(files[0]));
 
  std::vector<TFile*>   	inputFile;
  std::vector<TH1F*>   		inputHistos, outputHistos;
  inputFile             	.reserve( NMAX );
  inputHistos             	.reserve( NMAX );
  outputHistos             	.reserve( NMAX );

  std::cout<<std::endl;
  std::cout<<"** Looping over the following samples:                                    **"<<std::endl;
  std::cout<<std::endl;

  for(int i=0; i<sizeof(files) / sizeof(files[0]); i++) {
    TString     string_i  = std::string()+inpFold+files[i];
    inputFile   .push_back(new TFile(string_i));
    std::cout<< inputFile.at(i)->GetName() <<std::endl;

    TH1F* h; 
    inputFile[i]->GetObject(hist_name, h);
    if (i==0) 	outputHistos.push_back(h);
    else	{ h->Add(outputHistos[0]); h->Divide(outputHistos[0]); outputHistos.push_back(h); };

    std::cout<< h->GetNbinsX() << std::endl;
  } //for(int i=0; i<sizeof(files) / sizeof(files[0]); i++)

  TFile *f = new TFile("out_histos.root","RECREATE");
  TH1F *h1;
  TH1F *h2;
  TH1F *h3;
  TH1F *h4;
  TH1F *h5;
  TH1F *h6;
  TH1F *h7;
  TH1F *h8;
  TH1F *h9;
  TH1F *h10;
  TH1F *h11;

  h1  = (TH1F*)outputHistos[1]-> Clone(outputHistos[1]->GetName());
  h2  = (TH1F*)outputHistos[2]-> Clone(outputHistos[2]->GetName());
  h3  = (TH1F*)outputHistos[3]-> Clone(outputHistos[3]->GetName());
  h4  = (TH1F*)outputHistos[4]-> Clone(outputHistos[4]->GetName());
  h5  = (TH1F*)outputHistos[5]-> Clone(outputHistos[5]->GetName());
  h6  = (TH1F*)outputHistos[6]-> Clone(outputHistos[6]->GetName());
  h7  = (TH1F*)outputHistos[7]-> Clone(outputHistos[7]->GetName());
  h8  = (TH1F*)outputHistos[8]-> Clone(outputHistos[8]->GetName());
  h9  = (TH1F*)outputHistos[9]-> Clone(outputHistos[9]->GetName());
  h10 = (TH1F*)outputHistos[10]->Clone(outputHistos[10]->GetName());
  h11 = (TH1F*)outputHistos[11]->Clone(outputHistos[11]->GetName());
  std::string s1(files[1]+5, files[1]+11);
  h1->SetName(s1.c_str());
  std::string s2(files[2]+5, files[2]+11);
  h2->SetName(s2.c_str());
  std::string s3(files[3]+5, files[3]+11);
  h3->SetName(s3.c_str());
  std::string s4(files[4]+5, files[4]+11);
  h4->SetName(s4.c_str());
  std::string s5(files[5]+5, files[5]+11);
  h5->SetName(s5.c_str());
  std::string s6(files[6]+5, files[6]+11);
  h6->SetName(s6.c_str());
  std::string s7(files[7]+5, files[7]+11);
  h7->SetName(s7.c_str());
  std::string s8(files[8]+5, files[8]+11);
  h8->SetName(s8.c_str());
  std::string s9(files[9]+5, files[9]+11);
  h9->SetName(s9.c_str());
  std::string s10(files[10]+5, files[10]+11);
  h10->SetName(s10.c_str());
  std::string s11(files[11]+5, files[11]+11);
  h11->SetName(s11.c_str());

  f->Write();

  return 0;

} // int main()


